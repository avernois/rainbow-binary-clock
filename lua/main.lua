local data=1
local clock=2

local time = require("time")

local display = require("rainbow_binary_display")
display.init(clock,data)

local rainbow_clock = require("clock")
rainbow_clock.init(time, display)

rainbow_clock.start()

rainbow_clock = nil
package.loaded["clock"] = nil