wifi.sta.autoconnect(1)

abort = false

function doThings()
    dofile("main.lua")
end

function startup()
    if abort == true then
        print('startup aborted')
        return
    end
    print('in startup')
    doThings()
end

tmr.alarm(0,5000,0,startup)
