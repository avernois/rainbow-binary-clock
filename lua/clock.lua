local M = {}

local tmr = tmr

setfenv(1, M)

local display
local time
local refresh_rate = 100


local function update_time()
    now = time.get_zoned_time(1)
    display.display(now["hour"], now["min"], now["sec"])
end

local function auto_update_time()
    tmr.create():alarm(refresh_rate, tmr.ALARM_AUTO, update_time)
    
end

function M.init(time_module, time_display)
    time = time_module
    time.autosync()
    display = time_display
end

function M.start()
    time.autosync()
    time.sync_ntp(function ()
        
        update_time()
        auto_update_time()
    end)
end

return M