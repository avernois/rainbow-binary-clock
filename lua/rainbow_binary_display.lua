local M = {}
local print = print
local apa102 = apa102
local string_char = string.char

setfenv(1, M) -- for 5.1

local data
local clock

local off_alpha = 1
local on_alpha = 28

local bin = {
    [0] = {0, 0, 0, 0},
    [1] = {0, 0, 0, 1},
    [2] = {0, 0, 1, 0},
    [3] = {0, 0, 1, 1},
    [4] = {0, 1, 0, 0},
    [5] = {0, 1, 0, 1},
    [6] = {0, 1, 1, 0},
    [7] = {0, 1, 1, 1},
    [8] = {1, 0, 0, 0},
    [9] = {1, 0, 0, 1},
}

local reverse_bin = {
    [0] = {0, 0, 0, 0},
    [1] = {1, 0, 0, 0},
    [2] = {0, 1, 0, 0},
    [3] = {1, 1, 0, 0},
    [4] = {0, 0, 1, 0},
    [5] = {1, 0, 1, 0},
    [6] = {0, 1, 1, 0},
    [7] = {1, 1, 1, 0},
    [8] = {0, 0, 0, 1},
    [9] = {1, 0, 0, 1},
}


local function queue_number(number)
    local unit    = number%10
    local decimal = math.floor(number/10)
end

function M.init(clock_pin, data_pin)
    clock = clock_pin
    data = data_pin
end

local leds = {}

local function color (a, r, g, b)
    return string_char(a, b, g, r)
end

local function colored_number(number, nb_bits, on, off)
    local string = ""
    local bits = reverse_bin[number];
    
    for i = 1, nb_bits do
        if bits[i] == 0 then
            string = string..off
        else
            string = string..on
        end
    end

    return string
end

local function split_number(number)
    local unit   = number%10
    local decade = number/10

    return unit, decade
end


local function queue_hour(hour)
    local d_off = color(off_alpha,  75, 0, 130)
    local d_on = color(on_alpha,  75, 0, 130)
    local u_off = color(off_alpha,  0, 0, 255)
    local u_on = color(on_alpha,  0, 0, 255)
    
    local unit, decade = split_number(hour)

    local string = colored_number(decade, 2, d_on, d_off)
    string = string .. colored_number(unit, 4, u_on, u_off)

    return string
end

local function queue_minute(minute)
    local d_off = color(off_alpha, 0, 255, 0 )
    local d_on = color(on_alpha, 0, 255, 0)
    local u_off = color(off_alpha, 255, 255, 0 )
    local u_on = color(on_alpha, 255, 255, 0 )
    
    local unit, decade = split_number(minute)

    local string = colored_number(decade, 3, d_on, d_off)
    string = string .. colored_number(unit, 4, u_on, u_off)

    return string
end

local function queue_seconds(seconds)
    local d_off = color(off_alpha, 255, 127, 0 )
    local d_on = color(on_alpha, 255, 127, 0 )
    local u_off = color(off_alpha, 255, 0 , 0 )
    local u_on = color(on_alpha, 255, 0 , 0 )
    
    local unit, decade = split_number(seconds)

    local string = colored_number(decade, 3, d_on, d_off)
    string = string .. colored_number(unit, 4, u_on, u_off)

    return string
end


function M.display(hour, minute, seconds)
    
    local off = color(0, 0, 0, 0)
    
    local string = ""
    string = string ..queue_hour(hour)..queue_minute(minute)..queue_seconds(seconds)
    
    apa102.write(data, clock, string)
    apa102.write(data, clock, string)
end

return M
