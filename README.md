# Raibow binary clock

The Rainbow Binary Clock is a clock that tell time using binary encoding. And rainbow colors.
It's made frome wood, an esp8266, 20 rgb leds and other stuff.

![it's 19:05:50](images/19-05-50.small.jpeg)

## case plans

The plans for the clock can be found in the [svg/](svg/) directory.
It's meant to be cut in a single A4 sheet of 3mm wood (I used mdf but plywood would have work as fine).

## electronics

The project can be found in the [kicad/](kicad/) directory.


* U2: 1x esp8266 (esp12e in my case) on breakout board and 2 1x8 pin header
* U1: ht7333
* C1: 1x 47µF capacitor
* P1: 1x microusb connector (with a 1x5 pin header)
* P2: 1x3 pin header (used to connect to tx/rx/gnd)
* P3: 1x4 pin header (used to connect led strip)
* SW1: a switch (to connect GPIO0 to ground in order to flash the firmware)

* strips from a 144leds/meter apa102 strip (sk9822 will work too):
    * 1x20 leds

Note: you might need my [kicad library](https://gitlab.com/avernois/kicad-lib).
Note 2: I have not receive the pcb so it's not tested. Make/use with caution.

## code

Code can be found in [lua/](lua/). It's in lua and to be run on a [nodemcu firmware](http://nodemcu.readthedocs.io/en/master/).
It requires at least apa102, sntp, rtctime and net modules.

## configuration

### wifi credentials

There are no convenient interface to give your wifi credentials right now (it'll come). So you'll have to do it manually (nodemcu doc might help [wifi.sta.config()](http://nodemcu.readthedocs.io/en/master/en/modules/wifi/#wifistaconfig))
    
## Next

* provide a nice interface to wifi credentials
* provide an interface to set timezone
* automatically manage dst
* user config for colors
* make ws2812 version

## Licence

Sofware and plans are under MIT Licence.
