EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:crafting_labs
LIBS:strip_connector-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Led strip connector"
Date "2017-12-05"
Rev ""
Comp "Crafting Labs"
Comment1 "This is made for Gouge"
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L strip_input L1
U 1 1 5A270EAB
P 4350 2550
F 0 "L1" H 4250 2900 60  0000 C CNN
F 1 "strip_input" V 4150 2550 60  0000 C CNN
F 2 "Crafting labs:strip_input" H 4550 2150 60  0001 C CNN
F 3 "" H 4550 2150 60  0000 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4650 2350 5250 2350
Wire Wire Line
	5000 2500 4650 2500
Wire Wire Line
	4650 2650 5050 2650
Wire Wire Line
	4650 2800 5100 2800
$Comp
L CONN_01X04 P1
U 1 1 5A299F34
P 5450 2500
F 0 "P1" H 5450 2750 50  0000 C CNN
F 1 "CONN_01X04" V 5550 2500 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x04" H 5450 2500 50  0001 C CNN
F 3 "" H 5450 2500 50  0000 C CNN
	1    5450 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	5000 2500 5000 2450
Wire Wire Line
	5000 2450 5250 2450
Wire Wire Line
	5050 2650 5050 2550
Wire Wire Line
	5050 2550 5250 2550
Wire Wire Line
	5100 2800 5100 2650
Wire Wire Line
	5100 2650 5250 2650
$EndSCHEMATC
